import time
import smbus
import RPi.GPIO as GPIO ## Import GPIO library

#================================================
# Description
#
# Connect the Sensor to I2C (SDA, SCL) and GND
# Connect VDD to the specified GPIO of the Raspberrry Pi
# Set the input Data
# Run the programm
#
#================================================
# Input

PinNr_VDD       = 21    # Voltage Pin
i2c_address_old = 0x30  # Current I2C Address
i2c_address_new = 0x2A  # New I2C Address

#================================================
# Definitions

# setup for GPIOs
GPIO.setmode(GPIO.BCM)           ## Use board pin numbering
GPIO.setup(PinNr_VDD, GPIO.OUT)  ## Setup GPIO Pin [PinNr]

# get i2c-bus
i2c = smbus.SMBus(1)

#================================================
# Functions

def StatusDiagnosticsResponse(byte):
    byteBinRaw = bin(byte)
    byteBin    = byteBinRaw[2:]
    stat = '0b' + byteBin[0:2]
    diag = '0b' + byteBin[2:6]
    resp = '0b' + byteBin[6:8]
    return stat, diag, resp

def print_COMMANDMODE(readOut):
    stat, diag, resp = StatusDiagnosticsResponse(readOut[0])
    print("-> Status: %2s , Diagnostics: %4s , Response: %2s, "
          %(stat, diag, resp))
    
def print_EEPROM(readOut):
    stat, diag, resp = StatusDiagnosticsResponse(readOut[0])
    print("-> Status: %2s , Diagnostics: %4s , Response: %2s , EEPROM data: %s  %s"
          %(stat, diag, resp, hex(readOut[2]), hex(readOut[3])))

#================================================
# Main

def main():
    # power off
    print(">> Restart Sensor (power off/on) <<")
    GPIO.output(PinNr_VDD, False)
    # wait 5 seconds
    time.sleep(5)

    # power on
    GPIO.output(PinNr_VDD, True)
    time.sleep(0.003)
    # Enter command mode
    i2c.write_block_data(i2c_address_old, 0xA0,[0x00])
    print("\n>> Enter Command Mode <<")
    # readout the data
    readOut = i2c.read_i2c_block_data(i2c_address_old, 0,4)
    print("Out: %4s  %4s  %4s" %(hex(readOut[0]),hex(readOut[1]),hex(readOut[2])))
    print_COMMANDMODE(readOut)

    # Read the EEPROM
    i2c.write_block_data(i2c_address_old, 0x1C,[0x00])
    print("\n>> Read EEPROM <<")
    # readout the data
    readOut = i2c.read_i2c_block_data(i2c_address_old, 0,4)
    print("Out: %4s  %4s  %4s" %(hex(readOut[0]),hex(readOut[1]),hex(readOut[2])))
    print_EEPROM(readOut)

    # change to new i2c
    print("\n>> Change I2C Address <<")
    i2c.write_i2c_block_data(i2c_address_old, 0x5C,[0x00, i2c_address_new])
    time.sleep(10)

    # Read the EEPROM
    print("\n>> Read EEPROM <<")
    i2c.write_block_data(i2c_address_old, 0x1C,[0x00])
    # readout the data
    readOut = i2c.read_i2c_block_data(i2c_address_old, 0,4)
    print("Out: %4s  %4s  %4s" %(hex(readOut[0]),hex(readOut[1]),hex(readOut[2])))
    print_EEPROM(readOut)

    # Exit command mode
    print("\n>> Exit Command Mode <<")
    i2c.write_block_data(i2c_address_old, 0x80,[0x00])
    time.sleep(1)
    # readout the data (new address)
    readOut = i2c.read_i2c_block_data(i2c_address_new, 0,4)
    print("Out: %4s  %4s  %4s" %(hex(readOut[0]),hex(readOut[1]),hex(readOut[2])))
    print_COMMANDMODE(readOut)
    print("=> if no error: successfull!!")
    
    return True

#================================================
# Start-up command

if __name__ == "__main__":
    main()
