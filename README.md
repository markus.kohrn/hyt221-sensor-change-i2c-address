# HYT221-Sensor Change I2C Address
This is a small, easy to handle, program to change the I2C address of a HYT221 digital humidity-temperature-sensor.
Since all sensors of this type come with the default address 0x28, it is nessessary to change it, if you want to integrate multiple sensors in the I2C bus system.
I uploaded this small program, because when I had the mentioned problem, I was not able to find a plug-and-play-solution to it.
Since many others seem to have the same problem, I decided to share my solution.
Maybe this might save someone else a bit time and patience.

# Manual
1. Connect the sensor pins I2C (SDA, SCL) and GND to the Raspberry Pi accordingly.
2. Connect the VDD pin to the specified GPIO of the Raspberrry Pi
3. Set the input data
   - i2c_address_old: for new sensors it should be 0x28
   - i2c_address_new: chose any free address
   - PinNr_VDD: chose a free GPIO pin on your raspberry
4. Run the programm

**Note:** You can check which I2C addresses are currently in use by executing the following statement in the terminal:

_sudo i2cdetect -y 1_

The active addresses are named, while the otheres are blanked "--".
The same way you can identify the current address of the sensor you want to change. In that case do not connect the VDD pin to a free GIPO-pin of the board but to 3.3 V power supply.

## Specifications
- Language: Python 3
- Platform: Raspberry Pi 4B (might run on other boards)

## Installation
No installation is required, just run the programm in Python 3 on a Raspberry Pi.

## License
Open source

## Project status
Completed
